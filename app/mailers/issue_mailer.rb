class IssueMailer < ActionMailer::Base
  default from: "no-reply@bugbook.com"
  default to: "r.sellvan@quamble.com.my"

  def mandrill_client
    @mandrill_client ||= Mandrill::API.new MANDRILL_API_KEY
  end

  # def new_issue(issue)
  #   @issue = issue
  #   mail(subject: "New Issue: #{issue.title}")
  # end

  def new_issue(issue)
    template_name = "new-issue"
    template_content = []
    message = {
      to: [{email: User.find_by_id(issue.assignee).email}],
      subject: "New Issue: #{issue.title}",
      merge_vars: [
        {rcpt: User.find_by_id(issue.assignee).email,
         vars: [
            {name: "ISSUE_TITLE", content: issue.title},
            {name: "ISSUE_ASSIGNEE", content: User.find_by_id(issue.assignee).name},
            {name: "ISSUE_ASSIGNOR", content: User.find_by_id(issue.user_id).name},
            {name: "ISSUE_NUMBER", content: issue.id}
          ]
        }
      ]
    }
    mandrill_client.messages.send_template template_name, template_content, message
  end
end
