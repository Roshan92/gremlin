class Issue < ActiveRecord::Base
  include PublicActivity::Model
  # tracked owner: ->(controller, model) { controller && controller.current_user }

  belongs_to :user
  has_many :comments

  validates :title, :description, :assignee, :kind, :priority, presence: true

  KIND = %w(bug enhancement proposal task)
  PRIORITY = %w(trivial minor major critical blocker)
  STATUS = %w(new open resolved closed pending duplicate invalid)
  LOCATION = %w(dashboard contacts tasks messages projects)

  after_create :send_notification

  #add helper to model
  acts_as_votable

  def send_notification
    IssueMailer.new_issue(self).deliver
  end

  def votes
    self.get_upvotes.size - self.get_downvotes.size
  end

end
