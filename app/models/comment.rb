class Comment < ActiveRecord::Base
  include PublicActivity::Common
  # tracked owner: ->(controller, model) { controller && controller.current_user }

  belongs_to :issue

  #add helper to model
  acts_as_votable

  def votes
    self.get_upvotes.size - self.get_downvotes.size
  end
end
