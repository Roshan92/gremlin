class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  has_many :issues
  has_many :comments, through: :issues

  include Gravtastic
  gravtastic

  #add helper to model
  acts_as_votable

end
