class CommentsController < ApplicationController
  before_action :set_issue, only: [:new, :create, :index, :upvote, :downvote]
  before_action :set_comment, only: [:show, :edit, :update, :destroy, :upvote, :downvote]

  def index
    @comments = Comment.all
  end

  def show
  end

  def new
    @comment = Comment.new
  end

  def edit
  end

  def upvote
    @comment.upvote_by current_user
    redirect_to issue_path(@issue), notice: 'Comment is upvoted'
  end

  def downvote
    @comment.downvote_by current_user
    redirect_to issue_path(@issue), notice: 'Comment is downvoted'
  end

  def create
    @comment = @issue.comments.new(comment_params)

    respond_to do |format|
      if @comment.save
        @comment.create_activity :create, owner: current_user
        format.html { redirect_to issue_path(@issue), notice: 'Comment was successfully created.' }
        format.json { render :show, status: :created, location: @issue }
      else
        format.html { redirect_to issue_path(@issue), alert: "Comments can't be blank" }
        format.json { render json: @issue.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @comment.update(comment_params)
        format.html { redirect_to issue_path(@issue), notice: 'Comment was successfully updated.' }
        format.json { render :show, status: :ok, location: @issue }
      else
        format.html { render :edit }
        format.json { render json: @issue.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @comment.destroy
    # @comment.create_activity :destroy, owner: current_user
    respond_to do |format|
      format.html { redirect_to issue_path(@comment.issue), notice: 'Comment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_issue
      @issue = Issue.find(params[:issue_id])
    end

    def set_comment
      @comment = Comment.find(params[:id])
    end

    def comment_params
      params.require(:comment).permit(:user_id, :body, :issue_id)
    end
end
