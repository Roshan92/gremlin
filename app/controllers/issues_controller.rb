class IssuesController < ApplicationController
  before_action :set_issue, only: [:show, :edit, :update, :destroy, :upvote, :downvote]
  before_action :authenticate_user!

  # GET /issues
  # GET /issues.json
  def index
    @issues = Issue.order("created_at DESC")
  end

  # GET /issues/1
  # GET /issues/1.json
  def show
    @comments = @issue.comments.all
    @comment = @issue.comments.build
  end

  # GET /issues/new
  def new
    @issue = Issue.new
  end

  # GET /issues/1/edit
  def edit
  end

  def upvote
    @issue.upvote_by current_user
    redirect_to issue_path(@issue), notice: 'Issue is upvoted'
  end

  def downvote
    @issue.downvote_by current_user
    redirect_to issue_path(@issue), notice: 'Issue is downvoted'
  end

  # POST /issues
  # POST /issues.json
  def create
    @issue = current_user.issues.new(issue_params)

    respond_to do |format|
      if @issue.save
        @issue.create_activity :create, owner: current_user
        format.html { redirect_to @issue, notice: 'Issue was successfully created.' }
        format.json { render :show, status: :created, location: @issue }
      else
        format.html { redirect_to new_issue_path, alert: 'Please fill in all the required field marked with *' }
        format.json { render json: @issue.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /issues/1
  # PATCH/PUT /issues/1.json
  def update
    respond_to do |format|
      if @issue.update(issue_params)
        format.html { redirect_to @issue, notice: 'Issue was successfully updated.' }
        format.json { render :show, status: :ok, location: @issue }
      else
        format.html { render :edit }
        format.json { render json: @issue.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /issues/1
  # DELETE /issues/1.json
  def destroy
    @issue.destroy
    # @issue.create_activity :destroy, owner: current_user
    respond_to do |format|
      format.html { redirect_to @issue, notice: 'Issue was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_issue
      @issue = Issue.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def issue_params
      params.require(:issue).permit(:title, :description, :assignee, :kind, :priority, :location, :status)
    end
end
