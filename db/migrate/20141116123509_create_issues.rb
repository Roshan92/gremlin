class CreateIssues < ActiveRecord::Migration
  def change
    create_table :issues do |t|
      t.string :title
      t.text :description
      t.string :assignee
      t.string :kind
      t.string :priority

      t.timestamps
    end
  end
end
