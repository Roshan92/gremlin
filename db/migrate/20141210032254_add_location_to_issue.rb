class AddLocationToIssue < ActiveRecord::Migration
  def change
    add_column :issues, :location, :string
  end
end
