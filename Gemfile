source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.1.5'

# Creating fake data to test against
# gem 'faker'
gem 'haml'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.4'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.1.0'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
gem 'therubyracer',  platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'

# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0',          group: :doc

# Use Sass-powered Bootstrap
gem 'bootstrap-sass', '~> 3.3.0.1'

# Use postgresql as the database for Active Record
gem 'pg'

# CKEditor is a ready-for-use HTML text editor designed to simplify web content creation
gem "ckeditor"

gem "better_errors"

group :development do

  gem "binding_of_caller"
  # gem 'rack-mini-profiler'
  # gem "rails-erd"
  gem "spring"
  gem "haml-rails"
  # convert all erb to haml
  gem "erb2haml"
  # gem that helps with translations, start it in /locales and find it on: http://localhost:5050
  gem "iye"
end

# Rspec testing
group :development, :test do
  gem "rspec-rails", "~> 2.14.0"
  gem "factory_girl_rails", "~> 4.2.1"
  gem "pry-rails"
end

group :test do
  gem "faker", "~> 1.2.0"
  gem "capybara", "~> 2.1.0"
  gem "database_cleaner", "~> 1.0.1"
  gem "launchy", "~> 2.3.0"
  gem "selenium-webdriver", "~> 2.35.1"
  #gem "capybara-webkit"
  gem "rack_session_access"
end

gem 'rails_12factor', group: :production

#menu helper
gem "simple-navigation"

gem 'jquery-datatables-rails', git: 'git://github.com/rweng/jquery-datatables-rails.git'

gem 'simple_form'
gem 'bootstrap-datepicker-rails'

#browser capability detection
gem 'modernizr-rails'

# Faster webserver
gem "thin"

# Ssssht...
gem 'quiet_assets', :group => :development

#gem 'ancestry'

#paperclip for upload image
gem 'paperclip'

#Authentication
gem 'devise'

#mandrill api
gem 'mandrill-api', '~> 1.0.53', require: "mandrill"

#Adding Gravatar
gem 'gravtastic'

#notification
gem 'public_activity'

#voting for comments and issue
gem 'acts_as_votable'
