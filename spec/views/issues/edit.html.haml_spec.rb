require 'spec_helper'

describe "issues/edit" do
  before(:each) do
    @issue = assign(:issue, stub_model(Issue,
      :title => "MyString",
      :description => "MyText",
      :assignee => "MyString",
      :kind => "MyString",
      :priority => "MyString"
    ))
  end

  it "renders the edit issue form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", issue_path(@issue), "post" do
      assert_select "input#issue_title[name=?]", "issue[title]"
      assert_select "textarea#issue_description[name=?]", "issue[description]"
      assert_select "input#issue_assignee[name=?]", "issue[assignee]"
      assert_select "input#issue_kind[name=?]", "issue[kind]"
      assert_select "input#issue_priority[name=?]", "issue[priority]"
    end
  end
end
