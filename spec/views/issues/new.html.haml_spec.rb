require 'spec_helper'

describe "issues/new" do
  before(:each) do
    assign(:issue, stub_model(Issue,
      :title => "MyString",
      :description => "MyText",
      :assignee => "MyString",
      :kind => "MyString",
      :priority => "MyString"
    ).as_new_record)
  end

  it "renders new issue form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", issues_path, "post" do
      assert_select "input#issue_title[name=?]", "issue[title]"
      assert_select "textarea#issue_description[name=?]", "issue[description]"
      assert_select "input#issue_assignee[name=?]", "issue[assignee]"
      assert_select "input#issue_kind[name=?]", "issue[kind]"
      assert_select "input#issue_priority[name=?]", "issue[priority]"
    end
  end
end
