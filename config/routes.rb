Rails.application.routes.draw do

  devise_for :users
  mount Ckeditor::Engine => '/ckeditor'

    resources :users
    resources :activities
    resources :issues do
      member do
          put 'upvote', to: "issues#upvote"
          put 'downvote', to: "issues#downvote"
      end
      resources :comments do
        member do
          put 'upvote', to: "comments#upvote"
          put 'downvote', to: "comments#downvote"
        end
      end
    end

  root 'issues#index'
end
